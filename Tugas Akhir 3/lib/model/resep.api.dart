import 'dart:convert';
import 'package:flutter_ta/model/resep.api.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_ta/model/resep.dart';

class ResepApi {
//yumly baru
// const req = unirest("GET", "https://yummly2.p.rapidapi.com/feeds/list");

// req.query({
// 	"limit": "24",
// 	"start": "0"
// });

// req.headers({
// 	"X-RapidAPI-Key": "773b3de89amsh6a288af7201ebcbp14df71jsn52d26d3d77aa",
// 	"X-RapidAPI-Host": "yummly2.p.rapidapi.com",
// 	"useQueryString": true
// });

//yumly lama
// vor req = unirest('GET', "https://yumly2.p.rapidapi.com/feeds/list");

// req.query({
//   "start": "0",
//   "limit": "18",
//   "tag": "list.recipe.popular"
// });

// req.headers({
//   "x-rapidapi-key": "",,
//   "x-rapidapi-host": "yummly2.p.rapidapi.com",
//   "userQueryString": true
// });

  static Future<List<Resep>> getResep() async {
    var uri = Uri.https('tasty.p.rapidapi.com', 'recipes/list',
        {"from": "0", "size": "20", "tags": "under_30_minutes"});

    final response = await http.get(uri, headers: {
      "X-RapidAPI-Key": "773b3de89amsh6a288af7201ebcbp14df71jsn52d26d3d77aa",
      "X-RapidAPI-Host": "tasty.p.rapidapi.com",
      "useQueryString": "true"
    });

    Map data = jsonDecode(response.body);

    List _temp = [];

    for (var i in data['results']) {
      _temp.add(i);
    }

    return Resep.resepFromSnapshot(_temp);
  }
}
